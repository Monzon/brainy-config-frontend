import {inject} from 'aurelia-framework';
import {BrainyConfigRouter} from 'brainy-config-router';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(EventAggregator, BrainyConfigRouter)
export class App {
  spinnerAllowed = false;

  constructor(eventAggregator, brainyConfigRouter) {
    this.ea = eventAggregator;
    this.brainyConfigRouter = brainyConfigRouter;
  }

  configureRouter(config, router) {
    this.brainyConfigRouter.manage('Configuration of Brainy', config);
    this.router = router;
  }

  attached(){
    this.subscriber = this.ea.subscribe('showProgressHub', () => { this.showProgressHub(); } );
    this.subscriber = this.ea.subscribe('dismissProgressHub', () => { this.dismissProgressHub(); });
  }

  showProgressHub(){
    this.spinnerAllowed = true;
  }

  dismissProgressHub(){
    this.spinnerAllowed = false;
  }

  detached(){
    this.subscriber.dispose();
  }

}
